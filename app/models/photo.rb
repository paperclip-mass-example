require "lib/paperclip_processors/watermark"

class Photo < ActiveRecord::Base
  belongs_to :ad

  has_attached_file :data,
:styles => {
  :thumb => "100x100#",
  :first => {
    :processors => [:watermark, :thumbnail],
    :geometry => '300x250>',
    :watermark_path => Rails.root.join('public/images/watermark.png'),
    :position => 'SouthWest' },
  :large => {
    :processors => [:watermark, :thumbnail],
    :geometry => '640x480>',
    :watermark_path => Rails.root.join('public/images/watermark.png'),
    :position => 'SouthEast' }
}
end
